//  * Use asynchronous callbacks ONLY wherever possible.
//  * Error first callbacks must be used always.
//  * Each question's output has to be stored in a json file.
//  * Each output file has to be separate.

//  * Ensure that error handling is well tested.
//  * After one question is solved, only then must the next one be executed.
//  * If there is an error at any point, the subsequent solutions must not get executed.

//  * Store the given data into data.json
//  * Read the data from data.json
//  * Perfom the following operations.

//     1. Retrieve data for ids : [2, 13, 23].

const fs = require("fs");
const path = require("path");

const filePath = path.join(__dirname, "data.json");

function retrieveData(idsArr) {

    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            console.error("Error in reading the file",err);
        } else {
            data = JSON.parse(data);
            let res = data.employees.filter((employee) => {
                if (idsArr.includes(employee.id)) {
                    return employee;
                }
            });

            fs.writeFile("./output1.json", JSON.stringify(res), (err) => {
                if (err) {
                    console.error("Error in Writing the file",err);
                } else {
                    console.log("Done writing the file");
                }
            });
        }
    });
}

retrieveData([2, 13, 23]);

//     2. Group data based on companies.
//         { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}

function grouper(){

    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            console.error("Error in reading the file",err);
        } else {
            data = JSON.parse(data);
            let res = data.employees.reduce((acc,employeeData) => {
                if (acc.hasOwnProperty(employeeData.company)) {
                    acc[employeeData.company].push(employeeData)
                }
                else{
                    acc[employeeData.company] = [employeeData];
                }
                return acc
            },{});

            fs.writeFile("./output2.json", JSON.stringify(res), (err) => {
                if (err) {
                    console.error("Error in Writing the file",err);
                } else {
                    console.log("Done writing the file");
                }
            });
        }
    });
}

grouper();

//     3. Get all data for company Powerpuff Brigade

function companyNameSorter(companyName){

    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            console.error("Error in reading the file",err);
        } else {
            data = JSON.parse(data);

            let res = data.employees.filter((employeeData) => {
                if (employeeData.company == companyName) {
                    return employeeData;
                }
            });

            fs.writeFile("./output3.json", JSON.stringify(res), (err) => {
                if (err) {
                    console.error("Error in Writing the file",err);
                } else {
                    console.log("Done writing the file");
                }
            });
        }
    });
}

companyNameSorter("Powerpuff Brigade");

//     4. Remove entry with id 2.

function removeEntryWithNumber(number){

    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            console.error("Error in reading the file",err);
        } else {
            let parsedData = JSON.parse(data);

            let res = parsedData.employees.filter((employeeData) => {
                if (!(employeeData.id === number) ){
                    return employeeData;
                }
            });
            fs.writeFile("./output4.json", JSON.stringify(res), (err) => {
                if (err) {
                    console.error("Error in Writing the file",err);
                } else {
                    console.log("Done writing the file");
                }
            });
        }
    });
}

removeEntryWithNumber(2);

//     5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.


//     6. Swap position of companies with id 93 and id 92.
//     7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

//     NOTE: Do not change the name of this file
